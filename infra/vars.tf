variable "aws_region"{
  default = "us-gov-west-1"
}

variable "aws_ami"{
  #default="ami-5a740e3b"
  default="ami-1c81ce7d"
}

variable "instance_count" {
  default="1"
}
variable "ec2_type" {
  default="t2.small"
}
variable "ec2_key_name" {
  default="tam-2-govcloud"
}

variable "pub_key" {
  default="tam-2-govcloud"
}

variable "pub_key_bucket_name" {
  default="cie-mis"
}

variable "ssh_user" {
  default="ec2-user"
}

variable "ebs_vol_size" {
  default="10"
}

variable "private_subnet" {
  #TestVPC
  #default="subnet-9d3d7deb"
  #PRODVPC
  #default="subnet-ce688187"
  #CoreVPC
  default="subnet-3819e971"

}

variable "public_subnet" {
  #TestVPC
  #default="subnet-3b37774d"
  #PRODVPC
#  default="subnet-c606ef8f"
  #CoreVPC
  default="subnet-3819e971"
}

variable "vpc" {
  default = "vpc-262b2042"
}


variable "security_groups" {
  type    = list(string)
#  default = ["sg-12068674","sg-17e1216e","sg-d102c2a8"]
#  default = ["sg-6423e31d"]
  default = ["sg-fa15b383"]
}
