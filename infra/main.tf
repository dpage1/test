# Log into AWS
provider "aws" {
  region = "${var.aws_region}"
}

data "aws_s3_bucket_object" "pub-key" {
  bucket = "${var.pub_key_bucket_name}"
  key = "${var.pub_key}"
}

resource "aws_instance" "Gitlab-Runner-Core" {
  #count = "${var.instance_count}"
  ami           = "${var.aws_ami}"
  instance_type = "${var.ec2_type}"
  subnet_id = "${var.public_subnet}"
  security_groups = "${var.security_groups}"
  key_name = "${var.ec2_key_name}"
  root_block_device {
    volume_size = "${var.ebs_vol_size}"
  }
  timeouts {
    create = "60m"
    update = "60m"
  }
#  provisioner "remote-exec" {
#    inline = [
#               ansible-playbook -i hosts.txt -l runners gitlab_runner_install/site.yml
#      ]
#      connection {
#      type = "ssh"
#      user = "${var.ssh_user}"
#      private_key = "${file(var.pub_key)}"
#      host = "${self.public_ip}"
#      timeout = "10m"
#      }
#    }
   tags = {
    Name  = "Gitlab-Runner-Core"
    1067  = "N/A"
    Application = "Gitlab-Runner-Core"
    "Application Role" = "Service Management"
    "Cost Center" = "AFLCMC, AFRL"
    Email = "tameika@expansiagroup.com"
    Environment = "CORE"
    Group = "CIE, HBG, RIEBB"
    OS = "RHEL_7"
    Organization = "AF DCGS, AFRL"
    Owner = "Tameika Reed"
    Project = "CI/CD"
  }
}
