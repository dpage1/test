output "ec2_public_ip" {
 value = aws_instance.Gitlab-Runner-Core.public_ip
}

output "ec2_private_ip" {
 value = aws_instance.Gitlab-Runner-Core.private_ip
}

output "ec2_instance_id" {
 value = aws_instance.Gitlab-Runner-Core.instance_id
}
